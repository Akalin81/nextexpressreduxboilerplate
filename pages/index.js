import React from 'react';
import { connect } from 'react-redux';
import * as types from '../constants/ActionTypes'

import fetch from 'cross-fetch';

class Index extends React.Component {
  static async getInitialProps({ store, req }) {
    const isServer = !!req;
    if (isServer) {
      let ff = null;
      await fetch('myfavoriteApi')
        .then(response => response.json())
        .then(data => {
          console.log('got data')
          console.log(data.Result[0])
          ff = data.Result[0];
        })
        .catch(error => console.log('boofed with squi'));

      await store.dispatch({
        type: types.RECEIVE_SUCCESS,
        json: ff
      });
    }
    return {};
  }

  componentDidMount() {
    console.log('mounted');
    console.log(this.props);
  }

  componentDidUpdate = () => {
    console.log('updated');
    console.log(this.props);
  }

  render() {
    return <div>d: {JSON.stringify(this.props.common.data)}</div>;
  }
}
function mapStateToProps(state) {
  const { common } = state;
  return { common };
}

export default connect(mapStateToProps)(Index);

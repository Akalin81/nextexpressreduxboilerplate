import * as types from '../../constants/ActionTypes';
import fetch from 'cross-fetch';
//import * as config from '../constants/config';

export const requestLocation = () => ({
  type: types.REQUEST
});

export const receiveLocation = json => ({
  type: types.RECEIVE_SUCCESS,
  json
});

export const receiveError = error => ({
  type: types.RECEIVE_ERROR,
  error
});

export const getLatestData = () => {
  return dispatch => {
    fetch('myfavoriteapi.com')
      .then(response => response.json())
      .then(data => {
        dispatch({
          type: types.RECEIVE_SUCCESS,
          json: data.result[0]
        });
      })
      .catch(error => dispatch(receiveError(error)));
  };
};

import { combineReducers } from 'redux';
import common from './common';
import cup from './cup';
import league from './league';
import news from './news';

const rootReducer = combineReducers({
  common,
  cup,
  league,
  news
});

export default rootReducer;

import * as actions from '../../constants/ActionTypes';

const news = (
  state = {
    data: [],
    page: -1,
    dataLoading: false,
    dataFetched: false,
    fetchError: false
  },
  action
) => {
  switch (action.type) {
    case actions.RECEIVE_SUCCESS:
      return Object.assign({}, state, {
        fetchError: false,
      });
    default:
      return state;
  }
};

export default news;

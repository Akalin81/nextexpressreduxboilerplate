import * as actions from '../../constants/ActionTypes';

const league = (
  state = {
    leagueId: -1,
    fixture: [],
    points: [],
    year: -1,
    week: -1,
    dataLoading: false,
    dataFetched: false,
    fetchError: false,
    stats: {}
  },
  action
) => {
  switch (action.type) {
    case actions.RECEIVE_SUCCESS:
      return Object.assign({}, state, {
        fetchError: false,
      });
    default:
      return state;
  }
};

export default league;

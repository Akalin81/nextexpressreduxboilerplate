import * as actions from '../../constants/ActionTypes';

const cup = (
  state = {
    fixture: [],
    year: -1,
    dataLoading: false,
    dataFetched: false,
    fetchError: false,
    stats: {}
  },
  action
) => {
  switch (action.type) {
    case actions.RECEIVE_SUCCESS:
      return Object.assign({}, state, {
        fetchError: false,
      });
    default:
      return state;
  }
};

export default cup;

import * as actions from '../../constants/ActionTypes';

const common = (
  state = {
    maxWeek: -1,
    maxYear: -1,
    selWeek: -1,
    selYear: -1,
    selectedCategory: 1,
    data: null,
    fetchError: false
  },
  action
) => {
  switch (action.type) {
    case actions.RECEIVE_SUCCESS:
      {
        return Object.assign({}, state, {
          fetchError: false,
          data: action.json
        });
      }
    default:
      return state;
  }
};

export default common;

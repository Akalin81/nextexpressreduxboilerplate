webpackHotUpdate("static/development/pages/_app.js",{

/***/ "./redux/reducers/common.js":
/*!**********************************!*\
  !*** ./redux/reducers/common.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _constants_ActionTypes__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../constants/ActionTypes */ "./constants/ActionTypes.js");


var common = function common() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
    maxWeek: -1,
    maxYear: -1,
    selWeek: -1,
    selYear: -1,
    selectedCategory: 1,
    data: null,
    fetchError: false
  };
  var action = arguments.length > 1 ? arguments[1] : undefined;

  switch (action.type) {
    case _constants_ActionTypes__WEBPACK_IMPORTED_MODULE_0__["RECEIVE_SUCCESS"]:
      {
        return Object.assign({}, state, {
          fetchError: false,
          data: action.json
        });
      }

    default:
      return state;
  }
};

/* harmony default export */ __webpack_exports__["default"] = (common);

/***/ })

})
//# sourceMappingURL=_app.js.3157e3ac21630752296a.hot-update.js.map
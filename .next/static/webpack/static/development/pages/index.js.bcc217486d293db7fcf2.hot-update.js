webpackHotUpdate("static/development/pages/index.js",{

/***/ "./redux/actions/common.js":
/*!*********************************!*\
  !*** ./redux/actions/common.js ***!
  \*********************************/
/*! exports provided: requestLocation, receiveLocation, receiveError, getLatestData */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "requestLocation", function() { return requestLocation; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "receiveLocation", function() { return receiveLocation; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "receiveError", function() { return receiveError; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getLatestData", function() { return getLatestData; });
/* harmony import */ var _constants_ActionTypes__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../constants/ActionTypes */ "./constants/ActionTypes.js");
/* harmony import */ var cross_fetch__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! cross-fetch */ "./node_modules/cross-fetch/dist/browser-ponyfill.js");
/* harmony import */ var cross_fetch__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(cross_fetch__WEBPACK_IMPORTED_MODULE_1__);

 //import * as config from '../constants/config';

var requestLocation = function requestLocation() {
  return {
    type: _constants_ActionTypes__WEBPACK_IMPORTED_MODULE_0__["REQUEST"]
  };
};
var receiveLocation = function receiveLocation(json) {
  return {
    type: _constants_ActionTypes__WEBPACK_IMPORTED_MODULE_0__["RECEIVE_SUCCESS"],
    json: json
  };
};
var receiveError = function receiveError(error) {
  return {
    type: _constants_ActionTypes__WEBPACK_IMPORTED_MODULE_0__["RECEIVE_ERROR"],
    error: error
  };
};
var getLatestData = function getLatestData() {
  return function (dispatch) {
    cross_fetch__WEBPACK_IMPORTED_MODULE_1___default()('myfavoriteapi.com').then(function (response) {
      return response.json();
    }).then(function (data) {
      dispatch({
        type: _constants_ActionTypes__WEBPACK_IMPORTED_MODULE_0__["RECEIVE_SUCCESS"],
        json: data.result[0]
      });
    }).catch(function (error) {
      return dispatch(receiveError(error));
    });
  };
};

/***/ })

})
//# sourceMappingURL=index.js.bcc217486d293db7fcf2.hot-update.js.map